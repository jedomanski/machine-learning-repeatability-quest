########ARI calculation from generated clusters
###Input parameters : 
##$1 repertoire : file of studied clusters
##$2 save_file : backup file

###python CalculARI.py ${dir_data} "${dir_data}/ARI_set${g}.csv" &

##Libraries used
import os
import pathlib
import numpy as np
import pandas as pd
from sklearn.metrics import adjusted_rand_score as ARI
import glob
import sys

repertoire = sys.argv[1]
#'data/cluster_set/'
save_file = sys.argv[2]
#'data/ARI_set.csv'

##Retrieving the list of models and their numbers
liste_model = list(glob.glob(repertoire+'/*.csv'))
model_name=len(repertoire+'/df_cluster_m')
liste_num=[str(liste_model[i])[model_name:-4] for i in range(len(liste_model))]
lg = len(liste_model)
col=[i for i in range(lg)]
mat= pd.DataFrame(columns=liste_num)

##Calculating the ARI for each model in relation to the others
for i in range(lg):
    clusters1 = pd.read_csv(str(liste_model[i]))
    new_row=[]
    for j in range(lg):
        clusters2 = pd.read_csv(str(liste_model[j]))
        new_row.append(ARI(clusters1.to_numpy().flatten(), clusters2.to_numpy().flatten()))
        if (ARI(clusters1.to_numpy().flatten(), clusters2.to_numpy().flatten())>= 0.1) and (i!=j):
            print('GOOD MATCH'+str(i))
            print(str(liste_model[i])[model_name:-4],'vs', str(liste_model[j])[model_name:-4]+':\t'+str(ARI(clusters1.to_numpy().flatten(), clusters2.to_numpy().flatten())))
    mat.loc[len(mat)]=new_row

mat.index=liste_num

##Save the resulting matrix
mat.to_csv(save_file)  

