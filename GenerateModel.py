import sys
import DECscript
from DECscript import DeepEmbeddingClustering
from DECscript import ClusteringLayer
from DECscript import set_context
DECscript.generator=sys.argv[3]
DECscript.seed=sys.argv[4]


from keras.datasets import mnist
from keras.models import load_model, Model, Sequential
from sklearn.cluster import KMeans
import numpy as np
from sklearn.metrics import silhouette_score

save_num=sys.argv[1]
if (len(sys.argv)>=3):
    save_file=sys.argv[2]
else :
    save_file='models/model_'+str(save_num)+'.h5'
print(save_file)

def get_mnist():
    np.random.seed(1234) # set seed for deterministic ordering
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_all = np.concatenate((x_train, x_test), axis = 0)
    Y = np.concatenate((y_train, y_test), axis = 0)
    X = x_all.reshape(-1,x_all.shape[1]*x_all.shape[2])
    
    p = np.random.permutation(X.shape[0])
    X = X[p].astype(np.float32)*0.02
    Y = Y[p]
    return X, Y


X, Y  = get_mnist()

#DEC algo execution
print("Start of the algo section")
set_contexte()
print("model creation")
c = DeepEmbeddingClustering(n_clusters=10,input_dim=784,save_name=save_file)
print('model '+str(save_num))
#loading weights from another model
#c = DeepEmbeddingClustering(n_clusters=2, input_dim=df_features.shape[1],pretrained_weights='./weights/J_DEC_weights_model_100.h5',save_name=save_file)
c.initialize(X, finetune_iters=1000, layerwise_pretrain_iters=500)
print("clustering part")
c.cluster(X, y=Y)

#model prediction
print("prediction")
file_name=save_file
model = load_model(file_name, custom_objects={'ClusteringLayer': ClusteringLayer})
z = model.predict(X)
kmeans = KMeans(n_clusters=10, n_init=20).fit(z)
kl = kmeans.labels_

#silhouette score calculation
print("silhouette score calculation")
print('silhouette score = {}'.format(silhouette_score(z, kmeans.labels_)))

#For eDol dataset, KL Divergence gets to 0 directly for finetune_iters=10^4 and layerwise_pretrain_iters=5*10^3

