# DEC : Repeatability and reproducibility issues with the MNIST dataset

This project illustrates section 3 *Reaching software repeatability* of the article *Lost in machine learning hidden randomness: a quest for repeatability*. This README presents how to generate a model using the *Deep Embedding Clustering (DEC)* algorithm on the *MNIST* dataset, and how to generate different models by setting the context in different ways (*section 3.4 Targeting random sources*).

## How to generate a model
To generate a model useable in the VisualizationCluster.ipynb Notebook, follow these steps : \
#0 Pre-install the environment you'll be using \
It may be useful to do this in a virtual Python environment. \
https://www.freecodecamp.org/news/how-to-setup-virtual-environments-in-python/ \
	`$pip install -r requirements.txt`

#1 Generate the model \
See #File descriptions/ GenerateModel.py \
`$python example_bash.py <save_num> <save_file> <generator> <seed>`

## How to generate multiple models
To generate several models simultaneously, presenting all context configurations as presented in *section 3.4 Targeting random sources*, follow these steps :\
#0 Pre-install the environment you'll be using\
>It may be useful to do this in a virtual Python environment.\
> https://www.freecodecamp.org/news/how-to-setup-virtual-environments-in-python/ \

	`$pip install -r requirements.txt`
#1 Generate the models :

    $./scriptCompareMulti <seed> <dir> &

>This will generate 8 models using the same <seed>, along with their respective partitions, and a double-entry table showing their mutual *ARI*s. They will be saved in the directory <dir>
>
![Operation of the scriptCompareMulti file](https://gitlab.isima.fr/jedomanski/machine-learning-repeatability-quest/-/raw/master/CompareMulti.png?ref_type=heads)

## Project structure
```bash
├── models
│   ├── setExample
│   │   ├── models_mnist_110_s2048_01.h5
│   │   ├── models_mnist_110_s2048_02.h5
│   │   ├── models_mnist_011_s2048_03.h5
│   │   ├── models_mnist_011_s2048_04.h5
│   │   ├── models_mnist_001_s2048_05.h5
│   │   ├── models_mnist_001_s2048_06.h5
│   │   ├── models_mnist_000_s2048_07.h5
│   │   ├── models_mnist_000_s2048_08.h5
│   │   ├── models_mnist_111_s2048_01.h5
│   │   ├── models_mnist_111_s2048_02.h5
│   │   ├── models_mnist_101_s2048_03.h5
│   │   ├── models_mnist_101_s2048_04.h5
│   │   ├── models_mnist_010_s2048_05.h5
│   │   ├── models_mnist_010_s2048_06.h5
│   │   ├── models_mnist_100_s2048_07.h5
│   │   └── models_mnist_100_s2048_08.h5
├── data
│   ├── ARI_set2048.csv  
│   ├── cluster_setExample
│   │   ├── df_cluster_mmodels_mnist_110_s2048_01.csv
│   │   ├── df_cluster_mmodels_mnist_110_s2048_02.csv
│   │   ├── df_cluster_mmodels_mnist_011_s2048_03.csv
│   │   ├── df_cluster_mmodels_mnist_011_s2048_04.csv
│   │   ├── df_cluster_mmodels_mnist_001_s2048_05.csv
│   │   ├── df_cluster_mmodels_mnist_001_s2048_06.csv
│   │   ├── df_cluster_mmodels_mnist_000_s2048_07.csv
│   │   ├── df_cluster_mmodels_mnist_000_s2048_08.csv
│   │   ├── df_cluster_mmodels_mnist_111_s2048_01.csv
│   │   ├── df_cluster_mmodels_mnist_111_s2048_02.csv
│   │   ├── df_cluster_mmodels_mnist_101_s2048_03.csv
│   │   ├── df_cluster_mmodels_mnist_101_s2048_04.csv
│   │   ├── df_cluster_mmodels_mnist_010_s2048_05.csv
│   │   ├── df_cluster_mmodels_mnist_010_s2048_06.csv
│   │   ├── df_cluster_mmodels_mnist_100_s2048_07.csv 
│   │   └── df_cluster_mmodels_mnist_100_s2048_08.csv
├── generateModel.py
├── DECscript.py
├── scriptCompareMulti
├── SaveCluster.py
├── CalculARI.py
└── VizualisationCluster.ipynb

```
## File descriptions

### requirements.txt
   Summarizes the environment used
    `$pip install -r requirements.txt`

### GenerateModel.py
   Generates a clustering model using the DEC algorithm, and returns the silhouette score of its clustering.\
**input parameters :**\
bash :
- save_num (int)
> model registration number
- save_file (str)
>path of model to save, expected .h5 file
optional, default value : 'models/set_test/model_'+str(save_num)+'.h5'
- generator (000,100,010,001,110,101,011,111)
>model generation context :
>>000 sets no generator\
100 sets only the Mersenne Twister generator (Python)\
010 fixes only the Permuted Congruential Generator PCG (NumPy)\
001 only fixes Philox generator (TensorFlow)\
111 fixes all generators ...\
>Repeatability is guaranteed in configurations 111 and 101\
- seed (int)
>context-setting seed
> we'll take an integer under 2**32
                
   **variable :**
   - X
      >  incoming data set

    $python GenerateModel.py <save_num> <save_file> <generator> <seed>

### DECscript.py
définit 2 classes :
#### DeepEmbeddingClustering
Algorithm generating a model for classifying an input dataset.\
**input parameters :**
- n_clusters=10
>expected number of clusters
- input_dim=784 (pour le jeu de données MNIST)
- save_name=save_file (fichier .h5)
>path of the model to be saved
- pretrained_weights (fichier .h5)
>path for weights to be loaded
#### ClusteringLayer
Clustering layer which converts latent space Z of input layer into a probability vector for each cluster defined by its centre in
Z-space. Use Kullback-Leibler divergence as loss, with a probability target distribution.\
**input parameters :**
- output_dim: int > 0. 
>Should be same as number of clusters.
- input_dim: dimensionality of the input (integer).
>This argument (or alternatively, the keyword argument `input_shape`) is required when using this layer as the first layer in a model.
- weights: list of Numpy arrays to set as initial weights.
>The list should have 2 elements, of shape `(input_dim, output_dim)`and (output_dim,) for weights and biases respectively.
- alpha: parameter in Student's t-distribution. Default is 1.0.

**Input shape**\
2D tensor with shape: `(nb_samples, input_dim)`.\
**Output shape**\
2D tensor with shape: `(nb_samples, output_dim)`.

### scriptCompareMulti
Script producing 16 models, allowing comparison of different generator contextualizations.\
**Input parameters**
- seed (int)
> context-setting seed
> we'll take an integer under 2**32
- dir (str)
>backup directory
>
	$./scriptCompareMulti <seed> <dir> &
### SaveCluster.py
Generation of the cluster from a model on the MNIST dataset.\
**input parameters :**
- model_name : model studied
- save_rep : backup directory
- model_num : seed+clustering model number
### CalculARI.py
ARI calculation from generated clusters.\
**Input parameters :**
- repertoire : file of studied clusters
- save_file : backup file
### VizualisationCluster.ipynb
Notebook for generating clusters from previously generated models. Various visualization tools (UMAP, TSNE) help to better understand the clustering obtained. 

## Frequent errors
```bash
OSError: [Errno 5] Unable to synchronously create file (unable to lock file, errno = 5, error message = 'Input/output error')
```
*Solution :* à entrer dans le terminal
```bash
export HDF5_USE_FILE_LOCKING='FALSE'
```
> Written with [StackEdit](https://stackedit.io/).
> Jeanne Nautré 29/01/2024
