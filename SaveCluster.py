########Generation of the cluster from a model on the MNIST dataset
###input parameters : 
##$1 model_name : model studied
##$2 save_rep : backup directory
##$3 model_num : seed+clustering model number

##Libraries used
import sys

#Useful library for DEC
from keras.models import Model, Sequential, load_model
from sklearn.cluster import KMeans
import DECscript as keras_dec

#MINST dataset library
import numpy as np
from keras.datasets import mnist

#Evaluation libraries
from sklearn.metrics import adjusted_rand_score as ARI
import pandas as pd

##Dataset
#Mnist database retrieval function
def get_mnist():
    np.random.seed(1234) # set seed for deterministic ordering
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_all = np.concatenate((x_train, x_test), axis = 0)
    Y = np.concatenate((y_train, y_test), axis = 0)
    X = x_all.reshape(-1,x_all.shape[1]*x_all.shape[2])
    
    p = np.random.permutation(X.shape[0])
    X = X[p].astype(np.float32)*0.02
    Y = Y[p]
    return X, Y

X, Y  = get_mnist()

##Model prediction
model_name = sys.argv[1]
#"models/set/nom_du_fichier.h5"
model_num = sys.argv[3]
#s${seed}_${num}
save_rep = sys.argv[2]
#data/cluster_set/


file_name=model_name
#'models/set/'+model
model = load_model(file_name, custom_objects={'ClusteringLayer': keras_dec.ClusteringLayer})

z = model.predict(X)
kmeans = KMeans(n_clusters=10, n_init=20).fit(z)
kl = kmeans.labels_
kmeans_cluster_centers = kmeans.cluster_centers_

##ARI evaluation
print('ARI = {}'.format(ARI(kl, Y)))

##Cluster backup

df_cluster=pd.DataFrame(kl)
print(save_rep+"df_cluster_m"+str(model_num)+'.csv')
#df_cluster.to_csv(save_rep+"df_cluster_m"+str(model_num)+'.csv',header=False, index=False)
